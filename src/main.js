import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Vuelidate from "vuelidate/src";
import currencyFilter from "../filters/currency.filter";
import dateFilter from "../filters/date.filter";
import 'materialize-css/dist/js/materialize.min';
import Loader from "./components/app/Loader";
import messagePlugin from '@/utils/message.plugin'
import tooltipDirective from './directives/tooltip.directive'

import firebase from "firebase/app";
import 'firebase/auth'
import 'firebase/database'

Vue.use(messagePlugin);
Vue.config.productionTip = false;
Vue.filter('date', dateFilter);
Vue.filter('currency', currencyFilter);
Vue.use(Vuelidate);
Vue.directive('tooltip',tooltipDirective);
Vue.component('Loader', Loader);

firebase.initializeApp({
    apiKey: "AIzaSyDWXo_D7C0mMFypsRD9gjz5q2Bf4tOVUF0",
    authDomain: "vuecli-crm.firebaseapp.com",
    databaseURL: "https://vuecli-crm.firebaseio.com",
    projectId: "vuecli-crm",
    storageBucket: "vuecli-crm.appspot.com",
    messagingSenderId: "83340052606",
    appId: "1:83340052606:web:5e535b55c377ad8b79302f",
    measurementId: "G-R6L4C547BY"
});

let app;
firebase.auth().onAuthStateChanged(() => {
    if (!app) {
        app = new Vue({
            router,
            store,
            render: h => h(App)
        }).$mount('#app');
    }
});

